package com.chegg.hack.crypto;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.AbstractMap;
import java.util.Arrays;
import java.util.Enumeration;
import java.util.Map;
import java.util.Properties;

import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

public class EncryptionProcessor {

	private static final String IV = "AAAAAAAAAAAAAAAA";
	private static final String SEPARATOR = ",.";
	private static final EncryptionProcessor INSTANCE = new EncryptionProcessor();

	
	private EncryptionProcessor() {
	}

	public static EncryptionProcessor getInstance(){
		return INSTANCE;
	}

	public static byte[] sha256digest16(String password) throws NoSuchAlgorithmException, UnsupportedEncodingException {
		MessageDigest digest = MessageDigest.getInstance("SHA-256");
		digest.reset();
		digest.update(password.getBytes("UTF-8"));

		// so you have 32 bytes here
		byte[] b = digest.digest();

		// you can return it directly or you can cut it to 16 bytes
		return Arrays.copyOf(b, 16);
	}

	public byte[] encryptCredentials(Map.Entry<String, String> credentials) throws Exception {
		String pair = credentials.getKey() + SEPARATOR + credentials.getValue();
		return encrypt(pair, sha256digest16(generateKey()));
	}

	public Map.Entry<String, String> decrypt(byte[] cipherText) throws Exception {
		String res = decrypt(cipherText, sha256digest16(generateKey()));
		String[] pair = res.split(SEPARATOR);
		if (pair.length != 2) {
			throw new Exception("Could not split login password pair");
		}
		Map.Entry<String, String> entry = new AbstractMap.SimpleEntry<String, String>(pair[0], pair[1]);
		return entry;
	}

	private static byte[] encrypt(String plainText, byte[] encryptionKey) throws Exception {
		Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding", "SunJCE");
		SecretKeySpec key = new SecretKeySpec(encryptionKey, "AES");
		cipher.init(Cipher.ENCRYPT_MODE, key, new IvParameterSpec(IV.getBytes("UTF-8")));
		return cipher.doFinal(plainText.getBytes("UTF-8"));
	}

	private static String decrypt(byte[] cipherText, byte[] encryptionKey) throws Exception {
		Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding", "SunJCE");
		SecretKeySpec key = new SecretKeySpec(encryptionKey, "AES");
		cipher.init(Cipher.DECRYPT_MODE, key, new IvParameterSpec(IV.getBytes("UTF-8")));
		return new String(cipher.doFinal(cipherText), "UTF-8");
	}

	private String generateKey() {
		return getMacSpecificKeyPart() + " : " + getWinSpecificKeyPart() + " : " + getMacAddress();
	}

	private String getMacSpecificKeyPart() {
		Process p;
		try {
			p = Runtime.getRuntime().exec("ioreg -l");

			InputStream is = p.getInputStream();
			StringBuilder sb = new StringBuilder();
			int i1 = 0;
			while ((i1 = is.read()) != -1)
				if ((char) i1 != '|' && (char) i1 != ' ')
					sb.append((char) i1);
			InputStream fis = new ByteArrayInputStream(sb.toString().getBytes());
			Properties pro = new Properties();
			pro.load(fis);
			return pro.get("\"IOPlatformSerialNumber\"").toString()
					.substring(1, pro.get("\"IOPlatformSerialNumber\"").toString().length() - 1);
		} catch (IOException e) {
			return "no-mac";
		}
	}

	private String getWinSpecificKeyPart() {
		String NEWLINE = System.getProperty("line.separator");
		StringBuffer buffer = new StringBuffer();
		try {

			Process pb = new ProcessBuilder("cmd", "/c", "vol").start();
			InputStream in = pb.getInputStream();
			BufferedReader br = new BufferedReader(new InputStreamReader(in));
			String line;
			while ((line = br.readLine()) != null) {
				buffer.append(line + NEWLINE);
			}
		} catch (Exception e) {
			return "no-win";
		}
		return buffer.toString();
	}

	private String getMacAddress() {
		String emptyResult = "";
		try {
			Enumeration<NetworkInterface> addresses;
			addresses = NetworkInterface.getNetworkInterfaces();
			if (addresses.hasMoreElements()) {
				NetworkInterface ni = addresses.nextElement();
				if (ni != null) {
					byte[] mac = ni.getHardwareAddress();
					if (mac != null) {
						StringBuilder addr = new StringBuilder();
						for (int i = 0; i < mac.length; i++) {
							addr.append(String.format("%02X%s", mac[i], (i < mac.length - 1) ? "-" : ""));
						}
						return addr.toString();
					}
				}
			}
			return emptyResult;

		} catch (SocketException e) {
			return emptyResult;
		}

	}

}
