package com.chegg.hack.cia.tray;

import java.awt.AWTException;
import java.awt.Image;
import java.awt.MenuItem;
import java.awt.PopupMenu;
import java.awt.SystemTray;
import java.awt.Toolkit;
import java.awt.TrayIcon;
import java.awt.TrayIcon.MessageType;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import com.chegg.hack.cia.controllers.NotificationsController;

public class TrayHandler {


    private static TrayHandler INSTANCE = new TrayHandler();

    private final TrayIcon icon;

    private TrayHandler() {
        final SystemTray tray = SystemTray.getSystemTray();
        Toolkit toolkit = Toolkit.getDefaultToolkit();
        Image image = toolkit.getImage("images/LOGO-CIA.png");

        // TODO refactor tray icon impl
        icon = new TrayIcon(image, "CIA");
        icon.setImageAutoSize(true);

        try {
            tray.add(icon);
        } catch (AWTException e1) {
            // TODO add logger
        }

//        icon.addMouseListener(new MouseAdapter() {
//            @Override
//            public void mouseClicked(MouseEvent e) {
//                if (e.getButton() == MouseEvent.BUTTON1) {
//                    NotificationsController.getInstance().show();
//                }
//            }
//        });
        
        
        final PopupMenu popup = new PopupMenu();
        
        MenuItem exitItem = new MenuItem("Close");
        exitItem.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                tray.remove(icon);
                System.exit(0);
            }
        });
        popup.add(exitItem);
        
        icon.setPopupMenu(popup);
        
        
        
        icon.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                NotificationsController.getInstance().show();
            }
        });
    }

    public void displayMessageInTray(String caption, String text) {
        icon.displayMessage(caption, text, MessageType.WARNING);
    }

    public static TrayHandler getInstance() {
        return INSTANCE;
    }

}
