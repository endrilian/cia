/**
 * 
 */
package com.chegg.hack.cia;

import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Map.Entry;

import javax.swing.Timer;

import com.chegg.hack.cia.controllers.MainController;
import com.chegg.hack.cia.controllers.NotificationsController;
import com.chegg.hack.cia.controllers.SignInController;
import com.chegg.hack.cia.controllers.SignInControllerImpl;
import com.chegg.hack.cia.owa.CredentialsManager;
import com.chegg.hack.cia.owa.OwaInitializationException;
import com.chegg.hack.cia.owa.OwaProcessor;
import com.chegg.hack.cia.tray.TrayHandler;

/**
 * Launcher class
 */
public final class Launcher {

	private static MainController mainController = NotificationsController.getInstance();
	private static SignInController signInController = SignInControllerImpl.getInstance();

	private Launcher() {
	}

	private static void showSignInWindow(String errorMsg) {
		signInController.show();
		if (null != errorMsg) {
			signInController.showErrorMsg(errorMsg);
		}
	}

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {

				TrayHandler.getInstance();

				if (CredentialsManager.getInstance().isCredentialsSaved()) {

					mainController.show();
					EventQueue.invokeLater(new Runnable() {
						public void run() {
							Entry<String, String> credentials = CredentialsManager.getInstance().getCredentials();

							if (null == credentials) {
								showSignInWindow("Error while reading exisiting credentials");
								mainController.dispose();
							}

							try {
								OwaProcessor.getInstance().initializeCredentials(credentials.getKey(),
										credentials.getValue());
							} catch (OwaInitializationException e1) {
								showSignInWindow("Error while signIn with existing credentials");
								mainController.dispose();
							}
							Timer timer = new Timer(3000, new ActionListener() {

								@Override
								public void actionPerformed(ActionEvent e) {
									mainController.reloadData();

								}
							});
							timer.setRepeats(false);
							timer.start();

						}
					});

				} else {
					showSignInWindow(null);
				}
			}
		});
	}

}
