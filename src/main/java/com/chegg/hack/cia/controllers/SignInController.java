package com.chegg.hack.cia.controllers;

public interface SignInController {
	
	public void show();
	
	public void showErrorMsg(String errorMessage);
	
	public void disposeWindow();
	
	public void saveCredentials();

}
