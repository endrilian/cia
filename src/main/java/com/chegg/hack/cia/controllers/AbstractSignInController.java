package com.chegg.hack.cia.controllers;

import javax.swing.JFrame;

import com.chegg.hack.cia.owa.OwaInitializationException;
import com.chegg.hack.cia.owa.OwaProcessor;

public abstract class AbstractSignInController {
    
    public abstract JFrame getFrame();
    
    protected abstract AbstractNotificationsController createMainContorlerInstance();
    
    protected void makeSignIn(String login, String password) {
        OwaProcessor proc = OwaProcessor.getInstance();
        try {
            proc.initializeCredentials(login, password);
            //TODO add spinner here
            AbstractNotificationsController window = createMainContorlerInstance();
            window.getFrame().setVisible(true);
            getFrame().dispose();
        } catch (OwaInitializationException e1) {
            // TODO Auto-generated catch block
            e1.printStackTrace();
        }
    }

}
