package com.chegg.hack.cia.controllers;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.EventQueue;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.Toolkit;
import java.awt.Window.Type;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.AbstractMap;
import java.util.Map;

import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.Timer;

import com.chegg.hack.cia.owa.CredentialsManager;
import com.chegg.hack.cia.owa.OwaInitializationException;
import com.chegg.hack.cia.owa.OwaProcessor;

public class SignInControllerImpl implements SignInController {

    private static final String DEFAULT_PASSWORD_FIELD_VALUE = "Password";
    private static final String DEFAULT_EMAIL_FIELD_VALUE = "Email";

    private static final String EMAIL_PATTERN = "^[A-Za-z]*@chegg.com$";
    
    private static final SignInControllerImpl INSTANCE = new SignInControllerImpl(false);
    
    public static SignInControllerImpl getInstance(){
    	return INSTANCE;
    }


    private JFrame frame;
    private JTextField txtEmail;
    private JPasswordField txtPassword;
    private JButton btnSignIn;
    private JLabel lblCloseLabel;
    private JCheckBox chckbxStoreCredentials;

    private boolean isDesignMode;

    /**
     * Launch the application.
     */
    public static void main(String[] args) {
        EventQueue.invokeLater(new Runnable() {
            public void run() {
                try {
                    SignInControllerImpl window = new SignInControllerImpl(false);
                    window.frame.setVisible(true);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    private SignInControllerImpl() {
        this(true);
    }

    private SignInControllerImpl(boolean isDesignMode) {
        this.isDesignMode = isDesignMode;
        initialize();
    }

    /**
     * Initialize the contents of the frame.
     */
    private void initialize() {
        frame = new JFrame();
        frame.setAutoRequestFocus(false);
        frame.setResizable(false);
        frame.setType(Type.UTILITY);
        frame.setBounds(0, 0, 320, 480);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.getContentPane().setLayout(new BorderLayout(0, 0));
        frame.setUndecorated(true);
        frame.setLocationRelativeTo(null);

        JPanel panelTopBorder = new JPanel();
        FlowLayout fl_panelTop = (FlowLayout) panelTopBorder.getLayout();
        fl_panelTop.setHgap(10);
        fl_panelTop.setVgap(20);
        fl_panelTop.setAlignment(FlowLayout.RIGHT);
        frame.getContentPane().add(panelTopBorder, BorderLayout.NORTH);

        JPanel panelLeftBorder = new JPanel();
        FlowLayout fl_panelLeftBorder = (FlowLayout) panelLeftBorder.getLayout();
        fl_panelLeftBorder.setVgap(0);
        fl_panelLeftBorder.setHgap(20);
        frame.getContentPane().add(panelLeftBorder, BorderLayout.WEST);

        JPanel panelRightBorder = new JPanel();
        FlowLayout fl_panelRightBorder = (FlowLayout) panelRightBorder.getLayout();
        fl_panelRightBorder.setVgap(0);
        fl_panelRightBorder.setHgap(20);
        frame.getContentPane().add(panelRightBorder, BorderLayout.EAST);

        JPanel panelBottomBorder = new JPanel();
        FlowLayout fl_panelBottomBorder = (FlowLayout) panelBottomBorder.getLayout();
        fl_panelBottomBorder.setHgap(0);
        fl_panelBottomBorder.setVgap(15);
        frame.getContentPane().add(panelBottomBorder, BorderLayout.SOUTH);

        lblCloseLabel = new JLabel("");
        panelTopBorder.add(lblCloseLabel);

        JPanel panelElementsContainer = new JPanel();
        panelElementsContainer.setLayout(new BorderLayout(0, 20));
        frame.getContentPane().add(panelElementsContainer, BorderLayout.CENTER);

        JPanel panelLogoContainer = new JPanel();
        FlowLayout fl_panelLogoContainer = (FlowLayout) panelLogoContainer.getLayout();
        fl_panelLogoContainer.setHgap(0);
        fl_panelLogoContainer.setVgap(0);
        panelElementsContainer.add(panelLogoContainer, BorderLayout.NORTH);

        JLabel lblLogo = new JLabel("");
        lblLogo.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                try {
                Toolkit.getDefaultToolkit().beep();
                } catch (Exception ex) {
                    //TODO: log it
                }
            }
        });
        panelLogoContainer.add(lblLogo);

        JPanel panelSignIn = new JPanel();
        panelElementsContainer.add(panelSignIn, BorderLayout.CENTER);
        panelSignIn.setLayout(new GridLayout(4, 0, 0, 20));

        txtEmail = new JTextField();
        txtEmail.setBorder(BorderFactory.createMatteBorder(0, 0, 1, 0, Color.BLACK));
        panelSignIn.add(txtEmail);

        txtPassword = new JPasswordField();
        txtPassword.setBorder(BorderFactory.createMatteBorder(0, 0, 1, 0, Color.BLACK));
        panelSignIn.add(txtPassword);

        chckbxStoreCredentials = new JCheckBox("Keep me signed in");
        chckbxStoreCredentials.setVerticalAlignment(SwingConstants.TOP);
        panelSignIn.add(chckbxStoreCredentials);

        btnSignIn = new JButton("");
        btnSignIn.setBorderPainted(false);
        panelSignIn.add(btnSignIn);
        

        frame.setBackground(GuiStyleHelper.BACKGROUND_COLOR);
        panelTopBorder.setBackground(GuiStyleHelper.BACKGROUND_COLOR);
        panelLeftBorder.setBackground(GuiStyleHelper.BACKGROUND_COLOR);
        panelBottomBorder.setBackground(GuiStyleHelper.BACKGROUND_COLOR);
        panelRightBorder.setBackground(GuiStyleHelper.BACKGROUND_COLOR);
        panelElementsContainer.setBackground(GuiStyleHelper.BACKGROUND_COLOR);
        panelLogoContainer.setBackground(GuiStyleHelper.BACKGROUND_COLOR);
        panelSignIn.setBackground(GuiStyleHelper.BACKGROUND_COLOR);
        chckbxStoreCredentials.setBackground(GuiStyleHelper.BACKGROUND_COLOR);
        txtEmail.setBackground(GuiStyleHelper.BACKGROUND_COLOR);
        txtPassword.setBackground(GuiStyleHelper.BACKGROUND_COLOR);

        chckbxStoreCredentials.setFont(GuiStyleHelper.CHECKBOX_FONT);

        txtEmail.setForeground(Color.BLACK);
        txtEmail.setFont(GuiStyleHelper.INPUT_FIELD_FONT);
        txtEmail.setText(DEFAULT_EMAIL_FIELD_VALUE);

        txtPassword.setForeground(Color.BLACK);
        txtPassword.setFont(GuiStyleHelper.INPUT_FIELD_FONT);
        txtPassword.setText(DEFAULT_PASSWORD_FIELD_VALUE);
        



        // XXX: this helps to make my life easier during designing of this view
        lblCloseLabel.setIcon(new ImageIcon(
                "C:\\Users\\apolischuk\\Documents\\cia\\images\\icn-close.png"));
        lblLogo.setIcon(new ImageIcon(
                "C:\\Users\\apolischuk\\Documents\\cia\\images\\LOGO-CIA.png"));
        btnSignIn.setIcon(new ImageIcon(
                "C:\\Users\\apolischuk\\Documents\\cia\\images\\cia_login_button.png"));

        if (!isDesignMode) {
            lblCloseLabel.setIcon(new ImageIcon("images/icn-close.png"));
            lblLogo.setIcon(new ImageIcon("images/LOGO-CIA.png"));
            btnSignIn.setIcon(new ImageIcon("images/cia_login_button.png"));
        }


        lblCloseLabel.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent arg0) {
                System.exit(0);
            }
        });

        txtEmail.addFocusListener(constructJTextFieldFocusAdapter(DEFAULT_EMAIL_FIELD_VALUE));

        txtPassword.addFocusListener(constructJTextFieldFocusAdapter(DEFAULT_PASSWORD_FIELD_VALUE));

        btnSignIn.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent arg0) {
                if (!txtEmail.getText().matches(EMAIL_PATTERN)) {
                    txtEmail.setForeground(Color.RED);
                } else {
                	try {
    					OwaProcessor.getInstance().initializeCredentials(txtEmail.getText(), new String(txtPassword.getPassword()));
    					saveCredentials();
    					NotificationsController.getInstance().show();
    					Timer timer = new Timer(1500, new ActionListener() {

							@Override
							public void actionPerformed(ActionEvent e) {
								NotificationsController.getInstance().reloadData();

							}
						});
						timer.setRepeats(false);
						timer.start();
    					SignInControllerImpl.this.disposeWindow();
    				} catch (OwaInitializationException ex) {
    					SignInControllerImpl.this.showErrorMsg("Invalid credentials");
    				}
                }
            }
        });
    }

    
    

    private FocusAdapter constructJTextFieldFocusAdapter(final String defaultText) {
        return new FocusAdapter() {
            @Override
            public void focusGained(FocusEvent e) {
                JTextField jTextField = (JTextField) e.getSource();

                if (jTextField.getText().equals(defaultText)) {
                    jTextField.setText("");
                }
            }

            @Override
            public void focusLost(FocusEvent e) {
                JTextField jTextField = (JTextField) e.getSource();

                if (jTextField.getText().isEmpty()) {
                    jTextField.setText(defaultText);
                }
            }
        };
    }

	@Override
	public void show() {
		frame.setVisible(true);
		
	}

	@Override
	public void showErrorMsg(String errorMessage) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void disposeWindow() {
		frame.dispose();		
	}

	@Override
	public void saveCredentials() {
		if (chckbxStoreCredentials.isSelected()) {
			Map.Entry<String, String> credentials = new AbstractMap.SimpleEntry<String, String>(
					txtEmail.getText(), new String(txtPassword.getPassword()));
			CredentialsManager.getInstance().saveCredentials(credentials);
		}
		
	}
}
