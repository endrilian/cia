package com.chegg.hack.cia.controllers;

import java.awt.Color;
import java.awt.Window.Type;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.AbstractMap;
import java.util.Map;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;

import com.chegg.hack.cia.owa.CredentialsManager;
import com.chegg.hack.cia.owa.OwaInitializationException;
import com.chegg.hack.cia.owa.OwaProcessor;

/**
 * 
 */
public class LoginController implements SignInController {

    private final JFrame frame;
    private JTextField textFieldLogin;
    private JTextField textFieldPassword;
    private JCheckBox checkBoxRemember;
    private static final LoginController INSTANCE = new LoginController();
    
    public static LoginController getInstance(){
    	return INSTANCE;
    }


    /**
     * Create the application.
     */
    private LoginController() {
    	frame = new JFrame();
        initialize();
    }

    /**
     * Initialize the contents of the frame.
     */
    private void initialize() {        
        frame.setType(Type.UTILITY);
        frame.setResizable(false);
        frame.setBounds(100, 100, 227, 410);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.getContentPane().setLayout(null);
        
        JButton buttonLogin = new JButton("Login");
        buttonLogin.addActionListener(new ActionListener() {
        	public void actionPerformed(ActionEvent e) {     
        		try {
					OwaProcessor.getInstance().initializeCredentials(textFieldLogin.getText(), textFieldPassword.getText());
					saveCredentials();
					MainControllerImpl.getInstance().show();
				} catch (OwaInitializationException ex) {
					LoginController.this.showErrorMsg("Invalid credentials");
				}
        	}            
        });
        buttonLogin.setBounds(39, 308, 134, 29);
        frame.getContentPane().add(buttonLogin);
        
        textFieldLogin = new JTextField();
        textFieldLogin.setToolTipText("Please enter login here");
        textFieldLogin.setBounds(39, 137, 134, 28);
        frame.getContentPane().add(textFieldLogin);
        textFieldLogin.setColumns(10);
        
        textFieldPassword = new JTextField();
        textFieldPassword.setToolTipText("Please enter password here");
        textFieldPassword.setBounds(39, 217, 134, 28);
        frame.getContentPane().add(textFieldPassword);
        textFieldPassword.setColumns(10);
        
        checkBoxRemember = new JCheckBox("New check box");
        checkBoxRemember.setBounds(39, 257, 128, 23);
        frame.getContentPane().add(checkBoxRemember);
        
        JLabel labelLogin = new JLabel("Login:");
        labelLogin.setBounds(42, 109, 61, 16);
        frame.getContentPane().add(labelLogin);
        
        JLabel labelPassword = new JLabel("Password:");
        labelPassword.setBounds(49, 188, 124, 16);
        frame.getContentPane().add(labelPassword);
        
        JLabel labelImage = new JLabel("Image here");
        labelImage.setForeground(Color.DARK_GRAY);
        labelImage.setBackground(Color.DARK_GRAY);
        labelImage.setBounds(42, 29, 124, 44);
        frame.getContentPane().add(labelImage);
    }

    

	@Override
	public void show() {
		frame.setVisible(true);		
	}


	@Override
	public void disposeWindow() {
		frame.dispose();
		
	}

	@Override
	public void showErrorMsg(String errorMessage) {
		// TODO Auto-generated method stub
		
	}


	@Override
	public void saveCredentials() {
		if (checkBoxRemember.isSelected()) {
			Map.Entry<String, String> credentials = new AbstractMap.SimpleEntry<String, String>(
					textFieldLogin.getText(), textFieldPassword.getText());
			CredentialsManager.getInstance().saveCredentials(credentials);
		}

	}
}
