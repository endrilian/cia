package com.chegg.hack.cia.controllers;

public interface MainController {
	
	public void show();
	
	public void dispose();
	
	public void reloadData();

}
