package com.chegg.hack.cia.controllers;

import javax.swing.JFrame;

public abstract class AbstractNotificationsController {
    
    public abstract JFrame getFrame();

}
