package com.chegg.hack.cia.controllers;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.EventQueue;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.Toolkit;
import java.awt.Window.Type;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;

/**
 * 
 */
public class MessageController {

    private JFrame frame;

    private final String sender;
    private final String message;

    private boolean isDesignMode;

    /**
     * Launch the application.
     */
    public static void main(String[] args) {
        EventQueue.invokeLater(new Runnable() {
            public void run() {
                try {
                    MessageController window = new MessageController("Alex", "Hi! Hi! Hi!");
                    window.frame.setVisible(true);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    public MessageController(String sender, String message) {
        this.sender = sender;
        this.message = message;
        this.isDesignMode = false;
        initialize();
        try {
            Toolkit.getDefaultToolkit().beep();
        } catch (Exception ex) {
            // TODO: log it
        }
    }

    public MessageController() {
        this("apolischuk@chegg.com", "Hi man!\r\n\r\nYou have so many emails");
        this.isDesignMode = true;
        initialize();
    }
    
    public void show(){
    	this.frame.setVisible(true);
    }

    /**
     * Initialize the contents of the frame.
     */
    private void initialize() {
        frame = new JFrame();
        frame.setType(Type.UTILITY);
        frame.setBounds(100, 100, 300, 155);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setUndecorated(true);
        frame.setLocationRelativeTo(null);
        frame.getContentPane().setLayout(new BorderLayout(10, 10));

        JPanel panelHeader = new JPanel();
        frame.getContentPane().add(panelHeader, BorderLayout.NORTH);

        JPanel panelLeftBorder = new JPanel();
        frame.getContentPane().add(panelLeftBorder, BorderLayout.WEST);

        JPanel panelRightBorder = new JPanel();
        frame.getContentPane().add(panelRightBorder, BorderLayout.EAST);

        JPanel panelBottomBorder = new JPanel();
        frame.getContentPane().add(panelBottomBorder, BorderLayout.SOUTH);

        JPanel panelElements = new JPanel();
        frame.getContentPane().add(panelElements, BorderLayout.CENTER);
        panelElements.setLayout(new BorderLayout(5, 5));

        JTextArea txtrMessage = new JTextArea();
        txtrMessage.setFont(new Font("Verdana", Font.PLAIN, 14));
        txtrMessage.setWrapStyleWord(true);
        txtrMessage.setLineWrap(true);
        txtrMessage.setEditable(false);
        panelElements.add(txtrMessage, BorderLayout.CENTER);

        JLabel lblSender = new JLabel();
        lblSender.setForeground(Color.GRAY);
        lblSender.setFont(new Font("Verdana", Font.PLAIN, 14));
        panelElements.add(lblSender, BorderLayout.NORTH);

        panelHeader.setBackground(GuiStyleHelper.DARK_BACKGROUND_COLOR);
        panelLeftBorder.setBackground(GuiStyleHelper.DARK_BACKGROUND_COLOR);
        panelRightBorder.setBackground(GuiStyleHelper.DARK_BACKGROUND_COLOR);
        panelBottomBorder.setBackground(GuiStyleHelper.DARK_BACKGROUND_COLOR);
        panelElements.setBackground(GuiStyleHelper.DARK_BACKGROUND_COLOR);
        frame.setBackground(GuiStyleHelper.DARK_BACKGROUND_COLOR);
        frame.getContentPane().setBackground(GuiStyleHelper.DARK_BACKGROUND_COLOR);
        txtrMessage.setBackground(GuiStyleHelper.DARK_BACKGROUND_COLOR);

        JLabel lblClose = new JLabel("");
        lblClose.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                frame.dispose();
            }
        });
        panelHeader.setLayout(new FlowLayout(FlowLayout.RIGHT, 5, 5));
        lblClose.setFont(new Font("Tahoma", Font.PLAIN, 14));
        panelHeader.add(lblClose);

        lblClose.setIcon(new ImageIcon(
                "C:\\Users\\apolischuk\\Documents\\cia\\images\\icn-close.png"));

        if (!isDesignMode) {
            lblClose.setIcon(new ImageIcon("images/icn-close.png"));
        }

        lblSender.setText(sender);
        txtrMessage.setText(message);
    }

}
