package com.chegg.hack.cia.controllers;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Desktop;
import java.awt.EventQueue;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GradientPaint;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Window.Type;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.swing.BorderFactory;
import javax.swing.DefaultListCellRenderer;
import javax.swing.DefaultListModel;
import javax.swing.DefaultListSelectionModel;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.SwingConstants;
import javax.swing.Timer;

import com.chegg.hack.cia.external.ExternalMessage;
import com.chegg.hack.cia.external.HttpClient;
import com.chegg.hack.cia.notifications.NotificationManager;
import com.chegg.hack.cia.notifications.NotificationMessage;
import com.chegg.hack.cia.owa.CredentialsManager;
import com.chegg.hack.cia.owa.ImpMessage;
import com.chegg.hack.cia.owa.OwaProcessor;
import com.chegg.hack.cia.properties.PropertiesHandler;
import com.sun.media.sound.SF2SoundbankReader;

/**
 * 
 */
public class NotificationsController implements MainController {

	private JFrame frame;
	private JList<ImpMessage> listNotifications;
	private JLabel lblUnreadValue;
	private static final NotificationsController INSTANCE = new NotificationsController(false);

	private static SimpleDateFormat TODAY_DATE_FORMAT = new SimpleDateFormat("HH:mm");
	private static SimpleDateFormat EARLIER_TODAY_DATE_FORMAT = new SimpleDateFormat("MMM dd");

	private boolean isDesignMode;

	public static NotificationsController getInstance() {
		return INSTANCE;
	}

	 /**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					NotificationsController window = new NotificationsController();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	private NotificationsController() {
		this(true);
	}

	private NotificationsController(boolean isDesignMode) {
		this.isDesignMode = isDesignMode;
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setAutoRequestFocus(false);
		frame.setResizable(false);
		frame.setType(Type.UTILITY);
		frame.setBounds(0, 0, 382, 510);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(new BorderLayout(0, 0));

		JPanel panelHeader = new JPanel();
		FlowLayout fl_panelHeader = (FlowLayout) panelHeader.getLayout();
		fl_panelHeader.setHgap(25);
		fl_panelHeader.setVgap(0);
		frame.getContentPane().add(panelHeader, BorderLayout.NORTH);

		JLabel lblHeader = new JLabel("");
		panelHeader.add(lblHeader);

		JLabel lblClose = new JLabel("");
		lblClose.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				frame.setVisible(false);
			}
		});
		panelHeader.add(lblClose);

		JPanel panelBottom = new JPanel();
		FlowLayout fl_panelBottom = (FlowLayout) panelBottom.getLayout();
		fl_panelBottom.setHgap(20);
		fl_panelBottom.setVgap(0);
		frame.getContentPane().add(panelBottom, BorderLayout.SOUTH);

		JLabel lblRefresh = new JLabel("");
		lblRefresh.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				reloadData();
			}
		});
		panelBottom.add(lblRefresh);

		JLabel lblToSite = new JLabel("");
		lblToSite.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				try {
					Desktop.getDesktop().browse(new URI(PropertiesHandler.getInstance().getAggregatorUrl()));
				} catch (IOException | URISyntaxException e1) {
					// TODO proper handling
				}
			}
		});
		panelBottom.add(lblToSite);

		JPanel panelElements = new JPanel();
		frame.getContentPane().add(panelElements, BorderLayout.CENTER);
		panelElements.setLayout(new BorderLayout(10, 0));

		frame.setUndecorated(true);
		frame.setLocationRelativeTo(null);

		listNotifications = new JList<ImpMessage>();
		listNotifications.setVisibleRowCount(-1);
		listNotifications.setSelectionMode(DefaultListSelectionModel.SINGLE_SELECTION);
		listNotifications.addMouseListener(new MouseAdapter() {
			public void mouseClicked(MouseEvent evt) {
				@SuppressWarnings("unchecked")
				JList<ImpMessage> list = (JList<ImpMessage>) evt.getSource();
				if (evt.getClickCount() == 2) {

					// Double-click detected
					int index = list.locationToIndex(evt.getPoint());
					ImpMessage msg = (ImpMessage) list.getModel().getElementAt(index);
					try {
						Desktop.getDesktop().browse(new URI(msg.getUrl()));
						Timer timer = new Timer(10000, new ActionListenerImpl());
						timer.setRepeats(false);
						timer.start();
						
					} catch (IOException e2) {
						// TODO ADD logger
					} catch (URISyntaxException e2) {
						// TODO ADD logger
					}
				}
			}
		});
		panelElements.add(listNotifications, BorderLayout.CENTER);

		listNotifications.setCellRenderer(new ExtendedListCellRenderer());

		JPanel panel = new JPanel() {
			protected void paintComponent(Graphics g) {
				if (!isOpaque()) {
					super.paintComponent(g);
					return;
				}

				Graphics2D g2d = (Graphics2D) g;

				// Paint a gradient from top to bottom
				GradientPaint gp = new GradientPaint(0, 0, GuiStyleHelper.BACKGROUND_COLOR, 0, getHeight(),
						GuiStyleHelper.DARK_BACKGROUND_COLOR);

				g2d.setPaint(gp);
				g2d.fillRect(0, 0, getWidth(), getHeight());
			}
		};
		FlowLayout flowLayout = (FlowLayout) panel.getLayout();
		flowLayout.setVgap(10);
		panelElements.add(panel, BorderLayout.SOUTH);

		JPanel panelUnreadHeader = new JPanel();
		FlowLayout flowLayout_1 = (FlowLayout) panelUnreadHeader.getLayout();
		flowLayout_1.setAlignment(FlowLayout.LEFT);
		panelElements.add(panelUnreadHeader, BorderLayout.NORTH);

		JLabel lblEmpryPreUnread = new JLabel(" ");
		lblEmpryPreUnread.setHorizontalAlignment(SwingConstants.LEFT);
		panelUnreadHeader.add(lblEmpryPreUnread);

		JLabel lblUnread = new JLabel("Unread:");
		panelUnreadHeader.add(lblUnread);

		lblUnreadValue = new JLabel();
		panelUnreadHeader.add(lblUnreadValue);

		lblEmpryPreUnread.setFont(new Font("Verdana", Font.PLAIN, 20));

		lblUnreadValue.setFont(new Font("Verdana", Font.BOLD, 14));
		lblUnreadValue.setForeground(Color.GRAY);

		lblUnread.setForeground(Color.GRAY);
		lblUnread.setFont(new Font("Verdana", Font.BOLD, 14));

		frame.setBackground(GuiStyleHelper.DARK_BACKGROUND_COLOR);
		listNotifications.setBackground(GuiStyleHelper.BACKGROUND_COLOR);
		lblRefresh.setBackground(GuiStyleHelper.DARK_BACKGROUND_COLOR);
		lblToSite.setBackground(GuiStyleHelper.DARK_BACKGROUND_COLOR);
		panelBottom.setBackground(GuiStyleHelper.DARK_BACKGROUND_COLOR);
		panelElements.setBackground(GuiStyleHelper.DARK_BACKGROUND_COLOR);

		panelHeader.setBackground(GuiStyleHelper.CHEGG_ORANGE_COLOR);

		panelUnreadHeader.setBorder(BorderFactory.createMatteBorder(0, 0, 1, 0, Color.GRAY));

		// XXX: this helps to make my life easier during designing of this view
		lblHeader.setIcon(new ImageIcon("C:\\Users\\apolischuk\\Documents\\cia\\images\\img-app-header.png"));
		lblClose.setIcon(new ImageIcon("C:\\Users\\apolischuk\\Documents\\cia\\images\\icn-close.png"));
		lblRefresh.setIcon(new ImageIcon("C:\\Users\\apolischuk\\Documents\\cia\\images\\img-bottom-refresh.png"));
		lblToSite.setIcon(new ImageIcon("C:\\Users\\apolischuk\\Documents\\cia\\images\\img-bottom-web.png"));

		if (!isDesignMode) {
			lblHeader.setIcon(new ImageIcon("images/img-app-header.png"));
			lblClose.setIcon(new ImageIcon("images/icn-close.png"));
			lblRefresh.setIcon(new ImageIcon("images/img-bottom-refresh.png"));
			lblToSite.setIcon(new ImageIcon("images/img-bottom-web.png"));
		}
		
		Timer timer = new Timer(PropertiesHandler.getInstance().getRefreshTime() * 1000, new ActionListenerImpl());
		timer.setRepeats(true);
		timer.start();
	}

	private class ExtendedListCellRenderer extends DefaultListCellRenderer {
		@Override
		public Component getListCellRendererComponent(JList<?> list, Object value, int index, boolean isSelected,
				boolean cellHasFocus) {

			JList<ImpMessage> _list = (JList<ImpMessage>) list;
			ImpMessage message = _list.getModel().getElementAt(index);

			JPanel panelListItemComponentHolder = new JPanel();
			panelListItemComponentHolder.setOpaque(true);

			JLabel lblSubject = new JLabel();
			lblSubject.setVerticalAlignment(SwingConstants.BOTTOM);

			JLabel lblSender = new JLabel();
			lblSender.setVerticalAlignment(SwingConstants.TOP);

			JLabel lblSentDate = new JLabel();
			lblSentDate.setHorizontalAlignment(SwingConstants.CENTER);

			GroupLayout gl_panel = new GroupLayout(panelListItemComponentHolder);
			gl_panel.setHorizontalGroup(gl_panel.createParallelGroup(Alignment.LEADING).addGroup(
					gl_panel.createSequentialGroup()
							.addComponent(lblSentDate, GroupLayout.DEFAULT_SIZE, 66, Short.MAX_VALUE)
							.addPreferredGap(ComponentPlacement.UNRELATED)
							.addGroup(
									gl_panel.createParallelGroup(Alignment.TRAILING)
											.addComponent(lblSender, Alignment.LEADING, GroupLayout.DEFAULT_SIZE,
													GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
											.addComponent(lblSubject, Alignment.LEADING, GroupLayout.DEFAULT_SIZE, 260,
													Short.MAX_VALUE)).addGap(0)));
			gl_panel.setVerticalGroup(gl_panel
					.createParallelGroup(Alignment.LEADING)
					.addGroup(
							gl_panel.createSequentialGroup()
									.addComponent(lblSubject, GroupLayout.DEFAULT_SIZE, 25, Short.MAX_VALUE)
									.addPreferredGap(ComponentPlacement.RELATED)
									.addComponent(lblSender, GroupLayout.DEFAULT_SIZE, 25, Short.MAX_VALUE))
					.addComponent(lblSentDate, GroupLayout.DEFAULT_SIZE, 50, Short.MAX_VALUE));
			panelListItemComponentHolder.setLayout(gl_panel);

			panelListItemComponentHolder.setBackground(GuiStyleHelper.BACKGROUND_COLOR);
			panelListItemComponentHolder.setBorder(BorderFactory.createMatteBorder(0, 0, 1, 0, Color.LIGHT_GRAY));

			lblSubject.setForeground(Color.DARK_GRAY);
			lblSubject.setFont(new Font("Verdana", Font.BOLD, 12));

			lblSender.setForeground(Color.GRAY);
			lblSender.setFont(new Font("Verdana", Font.PLAIN, 12));

			lblSentDate.setForeground(Color.GRAY);
			lblSentDate.setFont(new Font("Verdana", Font.PLAIN, 14));

			lblSubject.setText(message.getSubject());
			lblSender.setText(message.getSenderName());
			lblSentDate.setText(getDateRepresentation(message.getDate()));

			return panelListItemComponentHolder;
		}

	}

	@SuppressWarnings("deprecation")
	private static String getDateRepresentation(Date dateSpecified) {
		Date today = new Date();
		today.setHours(0);
		today.setMinutes(0);
		today.setSeconds(0);
		if (dateSpecified.before(today)) {
			return EARLIER_TODAY_DATE_FORMAT.format(dateSpecified);
		}
		return TODAY_DATE_FORMAT.format(dateSpecified);

	}

	@Override
	public void show() {
		frame.setVisible(true);
		
	}

	@Override
	public void dispose() {
		frame.dispose();

	}

	@Override
	public void reloadData() {
		List<ImpMessage> messages = OwaProcessor.getInstance().getUnreadImportantMessages();
        if (null == messages || messages.size() == 0) {
            return;
        }
		DefaultListModel<ImpMessage> model = new DefaultListModel<ImpMessage>();
		for (ImpMessage impMessage : messages) {
			model.addElement(impMessage);
		}
		listNotifications.setModel(model);
		final String count = String.valueOf(messages.size());
		lblUnreadValue.setText(count);
		NotificationMessage message = new NotificationMessage("CIA", count + " unreaded messages");
		NotificationManager.getInstance().sendNotification(message);
		
		
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				HttpClient client = new HttpClient();
				List<ExternalMessage> messages = client.sendDataAndGetMessages(CredentialsManager.getInstance().getLogin(), count);
				for (ExternalMessage externalMessage : messages) {
					MessageController controller = new MessageController(externalMessage.getFrom(), externalMessage.getMessage());
					controller.show();
				}
				System.out.println(messages);
			}
		});

	}
	
	private class ActionListenerImpl implements ActionListener {
        
		@Override
		public void actionPerformed(ActionEvent e) {
			// TODO Auto-generated method stub
			
			NotificationsController.this.reloadData();
			
		}
      };
      
      

}
