package com.chegg.hack.cia.controllers;

import java.awt.Desktop;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;

import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JList;
import javax.swing.ListSelectionModel;

import com.chegg.hack.cia.owa.ImpMessage;
import com.chegg.hack.cia.owa.OwaProcessor;

public class MainControllerImpl implements MainController {

	private JFrame frame;
	private final JList<ImpMessage> list;
	private static final MainControllerImpl INSTANCE = new MainControllerImpl();
	
	/**
	 * Create the application.
	 */
	private MainControllerImpl() {
		list = new JList<ImpMessage>();
		initialize();
	}
	
	public static MainControllerImpl getInstance(){
		return INSTANCE;
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 603, 487);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		list.setBounds(44, 60, 427, 135);
        list.setSelectionMode(ListSelectionModel.SINGLE_INTERVAL_SELECTION);
        list.setLayoutOrientation(JList.VERTICAL);
        list.setVisibleRowCount(-1);
        
               
        frame.getContentPane().add(list);
        
        JButton buttonRefresh = new JButton("Refresh");
        buttonRefresh.addActionListener(new ActionListener() {
        	public void actionPerformed(ActionEvent e) {
        		reloadData();
        	}
        });
        
        buttonRefresh.setBounds(468, 16, 117, 29);
        frame.getContentPane().add(buttonRefresh);
	}

	public JFrame getFrame() {
		return this.frame;
	}

	@Override
	public void show() {
		this.frame.setVisible(true);
		
	}

	@Override
	public void dispose() {
		System.exit(0);
		
	}

	@Override
	public void reloadData() {
		//TODO REFACTOR: MOVE  TO PROPER PLACE
		DefaultListModel<ImpMessage> model = new DefaultListModel<ImpMessage>();

		List<ImpMessage> messages = OwaProcessor.getInstance().getUnreadImportantMessages();
		
		for (ImpMessage impMessage : messages) {
			model.addElement(impMessage);
		}
		list.setModel(model);
		
		list.addMouseListener(new MouseAdapter() {
            public void mouseClicked(MouseEvent evt) {
                JList<ImpMessage> list = (JList<ImpMessage>)evt.getSource();
                if (evt.getClickCount() == 2) {

                    // Double-click detected
                    int index = list.locationToIndex(evt.getPoint());
                    ImpMessage msg = (ImpMessage)list.getModel().getElementAt(index);
                    try {
            			Desktop.getDesktop().browse(new URI(msg.getUrl()));
            		} catch (IOException e2) {
            			// TODO Auto-generated catch block
            			e2.printStackTrace();
            		} catch (URISyntaxException e2) {
            			// TODO Auto-generated catch block
            			e2.printStackTrace();
            		}
                } 
            }
        });
		
	}
}
