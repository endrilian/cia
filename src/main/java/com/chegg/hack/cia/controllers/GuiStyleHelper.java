package com.chegg.hack.cia.controllers;

import java.awt.Color;
import java.awt.Font;

public class GuiStyleHelper {
    public static final Color BACKGROUND_COLOR = new Color(248, 250, 251);
    public static final Color DARK_BACKGROUND_COLOR = new Color(238, 238, 238);
    
    public static final Color CHEGG_ORANGE_COLOR = new Color(240, 125, 0);

    public static final Font INPUT_FIELD_FONT = new Font("Verdana", Font.PLAIN, 14);
    public static final Font CHECKBOX_FONT = new Font("Verdana", Font.PLAIN, 12);

}
