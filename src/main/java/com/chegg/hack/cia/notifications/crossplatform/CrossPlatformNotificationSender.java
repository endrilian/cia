package com.chegg.hack.cia.notifications.crossplatform;

import com.chegg.hack.cia.notifications.NotificationMessage;
import com.chegg.hack.cia.notifications.NotificationSender;
import com.chegg.hack.cia.tray.TrayHandler;

public class CrossPlatformNotificationSender implements NotificationSender {

	public CrossPlatformNotificationSender() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public void sendNotification(NotificationMessage message) {
		TrayHandler.getInstance().displayMessageInTray(message.getTitle(), message.getMessage());

	}

}
