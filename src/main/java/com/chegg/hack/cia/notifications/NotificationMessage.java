/**
 * 
 */
package com.chegg.hack.cia.notifications;

/**
 *
 */
public class NotificationMessage {
	
	private final String title;
	private final String message;

	/**
	 * 
	 */
	public NotificationMessage(String title, String message) {
		this.title = title;
		this.message = message;
	}

	/**
	 * @return the title
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * @return the message
	 */
	public String getMessage() {
		return message;
	}

}
