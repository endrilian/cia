/**
 * 
 */
package com.chegg.hack.cia.notifications;

import java.awt.Toolkit;

import com.chegg.hack.cia.notifications.crossplatform.CrossPlatformNotificationSender;
import com.chegg.hack.cia.notifications.osx.OsxNotificationSender;

/**
 *
 */
public class NotificationManager {
	
	private NotificationSender sender;
	private static NotificationManager INSTANCE = new NotificationManager();
	
	
	private NotificationManager() {
		if (System.getProperty("os.name").startsWith("Mac OS X")){
			sender = new OsxNotificationSender();
		} else {
			sender = new CrossPlatformNotificationSender();
		}
	}
	
	public void sendNotification(NotificationMessage message){
		sender.sendNotification(message);
        try {
            Toolkit.getDefaultToolkit().beep();
        } catch (Exception ex) {
            // TODO: log it
        }
	}
	
	public static NotificationManager getInstance(){
		return INSTANCE;
	}

}
