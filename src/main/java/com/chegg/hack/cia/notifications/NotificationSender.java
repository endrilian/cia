package com.chegg.hack.cia.notifications;

import java.util.Map;

public interface NotificationSender {
	
	public void sendNotification(NotificationMessage message);

}
