/**
 * 
 */
package com.chegg.hack.cia.notifications.osx;

import java.io.IOException;
import java.util.Map;

import com.chegg.hack.cia.notifications.NotificationMessage;
import com.chegg.hack.cia.notifications.NotificationSender;

/**
 *
 */
public class OsxNotificationSender implements NotificationSender {

	/**
	 * 
	 */
	public OsxNotificationSender() {
		// TODO Auto-generated constructor stub
	}

	/* (non-Javadoc)
	 * @see com.chegg.hack.cia.notifications.NotificationSender#sendNotification(java.util.Map)
	 */
	@Override
	public void sendNotification(NotificationMessage message) {
		try {
			Runtime.getRuntime().exec(
					new String[] { "./scripts/osx/SendAppleNotification", "-identifier", "lol", "-title",
							quote(message.getTitle()), "-subtitle", quote(message.getMessage()) });
		} catch (IOException e) {
			// TODO Add proper handling
			e.printStackTrace();
		}
	}
	
	private static String quote(String arg){
		return "\"" + arg + "\"";
	}

}
