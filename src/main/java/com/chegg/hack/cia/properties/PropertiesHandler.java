package com.chegg.hack.cia.properties;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.util.Properties;

public class PropertiesHandler {

	private static Properties props = new Properties();
	private static final String PATH = "./cia.properties";
	private static final PropertiesHandler INSTANCE = new PropertiesHandler();

	public enum PropertiesKeys {
		IS_ENCRYPTED("cia.password.isStored"),
		RELOAD_SECONDS("cia.reload.seconds"),
		AGGREGATOR_URL("cia.aggregator.url"),
		AGGREGATOR_POSTFIX("cia.aggregator.url.api.postfix");

		private String value;

		private PropertiesKeys(String value) {
			this.value = value;
		}

		public String getKeyName() {
			return this.value;
		}
	}

	private PropertiesHandler() {
		try {
			reloadProps();
		} catch (Exception e) {
			// TODO proper exception handling
		}
	}

	public static PropertiesHandler getInstance(){
		return INSTANCE;
	}
	
	// TODO proper exception handling
	private void reloadProps() throws Exception {
		FileInputStream file;

		// the base folder is ./, the root of the main.properties file

		// load the file handle for main.properties
		file = new FileInputStream(PATH);

		// load all the properties from this file
		props.load(file);

		// we have loaded the properties, so close the file handle
		file.close();
	}

	public String readProperty(PropertiesKeys propKey) {
		String propValue = props.getProperty(propKey.getKeyName());
		if (null == propValue) {
			// TODO add throwing exception here
		}
		return propValue;
	}

	public void writeProperty(PropertiesKeys propKey, String propValue) {

		try (OutputStream output = new FileOutputStream(PATH)) {
			props.setProperty(propKey.getKeyName(), propValue);
			props.store(output, null);
			reloadProps();

		} catch (Exception io) {// TODO Improve here
			io.printStackTrace();
		}
	}
	
	public boolean isStoredPassword(){
		return Boolean.parseBoolean(readProperty(PropertiesKeys.IS_ENCRYPTED));
	}
	
	public void setIsStoredPassword(boolean value){
		writeProperty(PropertiesKeys.IS_ENCRYPTED, String.valueOf(value));
	}
	
	public String getAggregatorUrl(){
		return readProperty(PropertiesKeys.AGGREGATOR_URL);
	}
	
	public String getAggregatorPostfix(){
		return readProperty(PropertiesKeys.AGGREGATOR_POSTFIX);
	}

	public Integer getRefreshTime(){
		return Integer.valueOf(readProperty(PropertiesKeys.RELOAD_SECONDS));
	}
		
}
