/**
 * 
 */
package com.chegg.hack.cia.owa;

/**
 *
 */
public class OwaInitializationException extends Exception{

	private static final long serialVersionUID = -111615407146912678L;

	/**
	 * 
	 */
	public OwaInitializationException() {
		super();
	}
	
	public OwaInitializationException(String message) {
		super(message);
	}

	public OwaInitializationException(String message, Throwable cause) {
		super(message, cause);
	}

	public OwaInitializationException(Throwable cause) {
		super(cause);
	}

}
