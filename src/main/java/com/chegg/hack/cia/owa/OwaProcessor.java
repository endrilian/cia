/**
 * 
 */
package com.chegg.hack.cia.owa;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import microsoft.exchange.webservices.data.autodiscover.IAutodiscoverRedirectionUrl;
import microsoft.exchange.webservices.data.core.ExchangeService;
import microsoft.exchange.webservices.data.core.PropertySet;
import microsoft.exchange.webservices.data.core.enumeration.misc.ExchangeVersion;
import microsoft.exchange.webservices.data.core.enumeration.property.BasePropertySet;
import microsoft.exchange.webservices.data.core.enumeration.property.Importance;
import microsoft.exchange.webservices.data.core.enumeration.property.WellKnownFolderName;
import microsoft.exchange.webservices.data.core.enumeration.search.LogicalOperator;
import microsoft.exchange.webservices.data.core.enumeration.search.SortDirection;
import microsoft.exchange.webservices.data.core.service.folder.Folder;
import microsoft.exchange.webservices.data.core.service.item.EmailMessage;
import microsoft.exchange.webservices.data.core.service.item.Item;
import microsoft.exchange.webservices.data.core.service.schema.EmailMessageSchema;
import microsoft.exchange.webservices.data.core.service.schema.FolderSchema;
import microsoft.exchange.webservices.data.core.service.schema.ItemSchema;
import microsoft.exchange.webservices.data.credential.ExchangeCredentials;
import microsoft.exchange.webservices.data.credential.WebCredentials;
import microsoft.exchange.webservices.data.search.FindFoldersResults;
import microsoft.exchange.webservices.data.search.FindItemsResults;
import microsoft.exchange.webservices.data.search.FolderView;
import microsoft.exchange.webservices.data.search.ItemView;
import microsoft.exchange.webservices.data.search.filter.SearchFilter;

/**
 *
 */
public class OwaProcessor {
	
	private static final OwaProcessor INSTANCE = new OwaProcessor();

	private boolean initialized;
	private final ExchangeService service;
	

	private OwaProcessor() {
		service = new ExchangeService(ExchangeVersion.Exchange2010_SP2);
	}
	
	static class RedirectionUrlCallback implements IAutodiscoverRedirectionUrl {
        public boolean autodiscoverRedirectionUrlValidationCallback(
                String redirectionUrl) {
            return redirectionUrl.toLowerCase().startsWith("https://");
        }
    }
	
	
	/**
	 * Get singleton instance
	 * @return 
	 */
	public static OwaProcessor getInstance(){
		return INSTANCE;
	}
	
	/**
	 * @return if initialized
	 */
	public boolean isInitialized(){
		return initialized;
	}
	
	public List<ImpMessage> getUnreadImportantMessages(){
		List<ImpMessage> messages = new LinkedList<>();
		
		try{
			//TODO refactor this for excluding from properties file
			List<SearchFilter> excludedFolders = new ArrayList<SearchFilter>();
	        excludedFolders.add(new SearchFilter.IsNotEqualTo(FolderSchema.DisplayName, "Deleted Items"));
	        excludedFolders.add(new SearchFilter.IsNotEqualTo(FolderSchema.DisplayName, "Drafts"));
			excludedFolders.add(new SearchFilter.IsNotEqualTo(FolderSchema.DisplayName, "Junk Email"));
			excludedFolders.add(new SearchFilter.IsNotEqualTo(FolderSchema.DisplayName, "Outbox"));
			excludedFolders.add(new SearchFilter.IsNotEqualTo(FolderSchema.DisplayName, "Sent Items"));
			excludedFolders.add(new SearchFilter.Not(new SearchFilter.ContainsSubstring(FolderSchema.DisplayName,
					"jira")));
			excludedFolders.add(new SearchFilter.Not(new SearchFilter.ContainsSubstring(FolderSchema.DisplayName,
					"stash")));
			excludedFolders.add(new SearchFilter.Not(
					new SearchFilter.ContainsSubstring(FolderSchema.DisplayName, "git")));
			SearchFilter excludedFoldersSearchFilter = new SearchFilter.SearchFilterCollection(LogicalOperator.And,
					excludedFolders);

			FindFoldersResults findResults = service.findFolders(WellKnownFolderName.MsgFolderRoot,
					new SearchFilter.SearchFilterCollection(LogicalOperator.And, new SearchFilter.IsEqualTo(
							FolderSchema.FolderClass, "IPF.Note"), excludedFoldersSearchFilter), new FolderView(
							Integer.MAX_VALUE));


            for (Folder folder : findResults.getFolders()) {
            	//TODO change to use config
                ItemView view = new ItemView(10);
                view.getOrderBy().add(ItemSchema.DateTimeSent, SortDirection.Descending);
                view.setPropertySet(new PropertySet(BasePropertySet.IdOnly, EmailMessageSchema.Subject, EmailMessageSchema.DateTimeSent, EmailMessageSchema.From));

                FindItemsResults<Item> items =
                    service.findItems(folder.getId(),
                        new SearchFilter.SearchFilterCollection(
                            LogicalOperator.And, new SearchFilter.IsEqualTo(EmailMessageSchema.Importance, Importance.High),
                        new SearchFilter.IsEqualTo(EmailMessageSchema.IsRead, Boolean.FALSE)), view);
                
                for (Item item : items) {
                    if(item instanceof EmailMessage) {
                        EmailMessage em = (EmailMessage) item;
                        ImpMessage message = new ImpMessage(em.getFrom().getName(), em.getSubject(), em.getId().toString(), em.getDateTimeSent());
                        messages.add(message);
                    }
              }     
                

            }
			
		} catch(Exception ex){
			//TODO add proper exception loggering
			System.out.println(ex);
		}
		
		return messages;
	}
	
	public void initializeCredentials(String login, String password) throws OwaInitializationException{
		if(initialized){
			throw new OwaInitializationException("Owa service already initialized");
		}
		
		ExchangeCredentials credentials = new WebCredentials(login, password);
		this.service.setCredentials(credentials);
		try {
			this.service.autodiscoverUrl(login, new RedirectionUrlCallback());
		} catch (Exception e) {
			throw new OwaInitializationException("Could not initialize service", e);
		}
		CredentialsManager.getInstance().setLogin(login);
		this.initialized = true;
	}

}
