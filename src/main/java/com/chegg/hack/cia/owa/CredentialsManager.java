package com.chegg.hack.cia.owa;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.Map;

import org.apache.commons.io.IOUtils;

import com.chegg.hack.cia.properties.PropertiesHandler;
import com.chegg.hack.crypto.EncryptionProcessor;

public class CredentialsManager {

	private static final CredentialsManager INSTANCE = new CredentialsManager();
	private static final String PWD_FILE_PATH = "./ecRts";
	private String login;
	
	
	private CredentialsManager() {
		
	}
	
	public static CredentialsManager getInstance(){
		return INSTANCE;
	}

	/**
	 * @return the credentialsSaved
	 */
	public boolean isCredentialsSaved() {
		return PropertiesHandler.getInstance().isStoredPassword();
	}

	
	
	private static void writePassword(byte[] data) {

		try (FileOutputStream fos = new FileOutputStream(PWD_FILE_PATH)) {
			fos.write(data);
		} catch (Exception e) {
			// TODO add logger
		}

	}

	private static byte[] readPassword() {
		try (FileInputStream is = new FileInputStream(PWD_FILE_PATH)) {
			return IOUtils.toByteArray(is);
		} catch (Exception e) {
			// TODO add logger
			return null;
		}

	}

	/**
	 * Could return null value if any exception occurred.
	 * 
	 * @return
	 */
	public Map.Entry<String, String> getCredentials() {
		Map.Entry<String, String> credentials = null;
		byte[] bytes = readPassword();
		try {
			credentials = EncryptionProcessor.getInstance().decrypt(bytes);
		} catch (Exception e) {
			// TODO add logger
		}
		return credentials;
	}

	public void saveCredentials(Map.Entry<String, String> credentials) {
		try {
			byte[] encryptedData = EncryptionProcessor.getInstance().encryptCredentials(credentials);
			writePassword(encryptedData);
			PropertiesHandler.getInstance().setIsStoredPassword(true);
		} catch (Exception e) {
			// TODO add logger
		}
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}
	
}
