package com.chegg.hack.cia.owa;

import java.util.Date;


public class ImpMessage {
	

	private static final String MESSAGE_LINK_TEMPLATE = "https://outlook.office365.com/owa/#ItemID=${msgId}&exvsurl=1&viewmodel=ReadMessageItem";
	
	private String senderName;
	private String subject;
	private String url;
	private Date date;
	
	public ImpMessage(String senderName, String subject, String id, Date date) {
		super();
		this.senderName = senderName;
		this.subject = subject;
		this.url = MESSAGE_LINK_TEMPLATE.replace("${msgId}", id);
		this.date = date;
	}
	
	/**
	 * @return the senderName
	 */
	public String getSenderName() {
		return senderName;
	}

	/**
	 * @return the subject
	 */
	public String getSubject() {
		return subject;
	}

	/**
	 * @return the url
	 */
	public String getUrl() {
		return url;
	}
	
	/**
	 * @return the date
	 */
	public Date getDate() {
		return date;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("FROM:");
		builder.append(senderName);
		builder.append(" | SUBJECT:");
		builder.append(subject);
		return builder.toString();
	}

}
