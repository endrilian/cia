package com.chegg.hack.cia.external;

public class ExternalMessage {
	
	private final String id;
	private final String email;
	private final String from;
	private final String message;
	private final String createdAt;
	private final String updatedAt;
	
	
	public ExternalMessage(String id, String email, String from, String message, String createdAt, String updatedAt) {
		super();
		this.id = id;
		this.email = email;
		this.from = from;
		this.message = message;
		this.createdAt = createdAt;
		this.updatedAt = updatedAt;
	}


	/**
	 * @return the id
	 */
	public String getId() {
		return id;
	}


	/**
	 * @return the email
	 */
	public String getEmail() {
		return email;
	}


	/**
	 * @return the from
	 */
	public String getFrom() {
		return from;
	}


	/**
	 * @return the message
	 */
	public String getMessage() {
		return message;
	}


	/**
	 * @return the createdAt
	 */
	public String getCreatedAt() {
		return createdAt;
	}


	/**
	 * @return the updatedAt
	 */
	public String getUpdatedAt() {
		return updatedAt;
	}

	

}
