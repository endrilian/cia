/**
 * 
 */
package com.chegg.hack.cia.external;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.codehaus.jackson.JsonFactory;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.type.TypeReference;

import com.chegg.hack.cia.properties.PropertiesHandler;

/**
 *
 */
public class HttpClient {
	
	private static final String URL = PropertiesHandler.getInstance().getAggregatorUrl() + PropertiesHandler.getInstance().getAggregatorPostfix();

	/**
	 * 
	 */
	public HttpClient() {
		// TODO Auto-generated constructor stub
	}
	
	public String sendPost(Map<String, String> params) throws Exception {
		 
		URL obj = new URL(URL);
		HttpURLConnection con = (HttpURLConnection) obj.openConnection();
 
		//add reuqest header
		con.setRequestMethod("POST");
//		con.setRequestProperty("Accept-Language", "en-US,en;q=0.5");
		
		StringBuilder paramsBuilder = new StringBuilder();
		for (String key : params.keySet()) {
			String value = params.get(key);
			if(value == null){
				continue;
			}
			paramsBuilder.append(key).append("=").append(value).append("&");
		}
		if(paramsBuilder.length() > 0){
			paramsBuilder.deleteCharAt(paramsBuilder.length()-1);
		}
 
		String urlParameters = paramsBuilder.toString();
 
		// Send post request
		con.setDoOutput(true);
		DataOutputStream wr = new DataOutputStream(con.getOutputStream());
		wr.writeBytes(urlParameters);
		wr.flush();
		wr.close();
 
		int responseCode = con.getResponseCode();
 
		if(responseCode == 200){
			BufferedReader in = new BufferedReader(
					new InputStreamReader(con.getInputStream()));
			String inputLine;
			StringBuffer response = new StringBuffer();
			
			while ((inputLine = in.readLine()) != null) {
				response.append(inputLine);
			}
			in.close();
			//print result
			return response.toString();
		}
		if(responseCode == 204){
			return null;
		}
		throw new Exception("Request unsuccessful");
	}
	
	public List<ExternalMessage> sendDataAndGetMessages(String email, String unreadedCount){
		List<ExternalMessage> messages = new LinkedList<ExternalMessage>();
		Map<String, String> paramsMap = new HashMap<String, String>();
		paramsMap.put("email", email);
		paramsMap.put("unread", unreadedCount);
		
		try {
			String response = sendPost(paramsMap);
			JsonFactory factory = new JsonFactory(); 
		    ObjectMapper mapper = new ObjectMapper(factory); 
		    TypeReference<HashMap<String,Object>> typeRef 
		            = new TypeReference<HashMap<String,Object>>() {};

		    HashMap<String, Object> o = mapper.readValue(response, typeRef);
		    @SuppressWarnings("unchecked")
			List<Map<String, Object>> list = (List<Map<String, Object>>) o.get("messages");
		    
		    for (Map<String, Object> map : list) {
				ExternalMessage msg = convertMapToMessage(map);
				messages.add(msg);
			}
			
		} catch (Exception e) {
			return new ArrayList<ExternalMessage>();
		}
		return messages;
		
	}
	
	/**
	 * Could return null values
	 * @param map
	 * @return
	 */
	private ExternalMessage convertMapToMessage(Map<String, Object> map){
		ExternalMessage msg  = null;
		try {
			String id = safeGetValue(map, "id");
			String email = safeGetValue(map, "email");
			String from = safeGetValue(map, "from");
			String message = safeGetValue(map, "message");
			String createdAt = safeGetValue(map, "created_at");
			String updatedAt = safeGetValue(map, "updated_at");			
			msg = new ExternalMessage(id, email, from, message, createdAt, updatedAt);
		} catch (Exception e) {
			//TODO change handling
		}
		return msg;
		
	}
	
	private String safeGetValue(Map<String, Object> map, String key) throws Exception {
		String value = String.valueOf(map.get(key));
		if(null == value){
			throw new Exception("null value");
		}
		return value;
	}
	
	private static void getMessages(){
		JsonFactory factory = new JsonFactory(); 
	    ObjectMapper mapper = new ObjectMapper(factory); 
	    String json = "{\"key\": [{\"key2\":\"value\"}]}";
	    TypeReference<HashMap<String,Object>> typeRef 
	            = new TypeReference<HashMap<String,Object>>() {};

	    HashMap<String, Object> o;
		try {
			o = mapper.readValue(json, typeRef);
			List<Map<String, Object>> list = (List<Map<String, Object>>) o.get("key");
			System.out.println(list);
			System.out.println("Got " + o); 
		} catch (JsonParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JsonMappingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
	}
	
	public static void main(String[] args) {
		getMessages();
	}
	

}
